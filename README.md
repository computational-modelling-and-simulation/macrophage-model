# COVID19 Macrophage model


The collection of files within the repository summarizes the steps we followed to generate a COVID19 Macrophage model. The model represents the behavior and response of macrophages during the course of accute COVID19. This repository has been organized as follows:
- **CasQ files**: Boolean models (Type 1 interferon response, ACE/ACE2 axes, and NLRP3 inflammasome pathway) were automatically obtained from their corresponding process diagrams using CasQ (SBML-qual format). All process diagrams are available at the [public Gitlab](https://git-r3lab.uni.lu/covid/models/-/tree/master/Curation) repository of the COVID19 Disease Map community.
- **GINsim files**: this folder contains the following subfolder:
  1. CasQ processed: CasQ files in zginml format.
  2. Macrophage-specific modules: refined and adjusted small modules in a cell-specific manner.
  3. Macrophage models: the polarization macrophage model and the COVID19-specific macrophage model.
- **Analysis files**: this folder contains the following files:
  1. The CoLoMoTo Jupyter Notebook: displays the steps followed in this project (each diagram and Boolean module), the final model description, and the analysis of the attractors.
  2. The summarizing figure of the project: displays the process and results of this work (the final COVID19 Macrophage model and the heatmap of the reached attractors).
  3. The Phenotypic node analysis folder: contains a summary Excel file with the output nodes and selected GOterms information, and the results of the ClueGO enrichment analysis. Also, it contains the raw results from the enrichment analysis, i.e., the ClueGO tables and the pie charts.